### getting the current active savestate data

in console execute the following command
```js
copy(localStorage.getItem("savestate"))
```
paste the result to a file and  save the file as .json