import JSONCResourceFile from "/GameTrackerJS/data/JSONCResourceFile.js";

export default await JSONCResourceFile.create("/logic/vanilla.min.json");
